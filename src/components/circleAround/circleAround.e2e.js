/* eslint-disable @typescript-eslint/explicit-function-return-type */
import puppeteer from 'puppeteer';
// import { PuppeteerHelper } from '../../testutil/puppeteerHelper';

const HEADLESS = true;
jest.setTimeout(30000);

/*
 NOTE that you must serve the demo page on port 3000 to make this test work
*/
describe('circle around component', () => {
  let browser;
  let page;
  // let helper;

  beforeEach(async () => {
    browser = await puppeteer.launch({
      headless: HEADLESS,
      args: ['--disable-web-security'] // mainly to disable OPTIONS preflight
    });
    page = await browser.newPage();
    // helper = new PuppeteerHelper(page, 'circle-around');
    await page.goto('http://localhost:3000/src/components/circleAround/circleAround.html');
  });

  afterEach(async () => {
    await browser.close();
  });

  it('First test', async () => {
    // TODO
    expect(true).toBe(true);
  });
});
