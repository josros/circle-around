import { LitElement, html, css, customElement, TemplateResult, property, CSSResult } from 'lit-element';

import * as THREE from 'three';

// https://threejs.org/docs/#examples/en/loaders/GLTFLoader
import { GLTFLoader, GLTF } from 'three/examples/jsm/loaders/GLTFLoader';
import { DRACOLoader } from 'three/examples/jsm/loaders/DRACOLoader';

// https://threejs.org/docs/#examples/en/controls/OrbitControls
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js';
import { Vector3 } from 'three';

import '@material/mwc-icon-button-toggle';
import '@material/mwc-linear-progress';

export interface Vector3D {
  x: number;
  y: number;
  z: number;
}

@customElement('circle-around')
export class CircleAround extends LitElement {
  private static readonly CIRCLE_SPEED = 2;

  @property({ type: String })
  private gltfFile!: string;

  @property({ type: Number })
  private width!: number;

  @property({ type: Number })
  private height!: number;

  @property({ type: Number })
  private groundColorHex!: number;

  @property({ type: String })
  private groundTexture?: string;

  @property({ type: Number })
  private backgroundColorHex!: number;

  @property({ type: Object })
  private translationVector!: Vector3D;

  @property({ type: Object })
  private rotationVector!: Vector3D;

  /**
   * scales dimensions x, y, z of the object loaded
   */
  @property({ type: Number })
  private scaleFactor!: number;

  @property({ type: Boolean })
  private debug?: boolean;

  // INTERNAL

  @property({ type: Object, attribute: false })
  private object!: GLTF;

  @property({ type: Object, attribute: false })
  private canvas!: HTMLCanvasElement;

  private doCircle = true;

  // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
  private initLogFunction = (function() {
    return Function.prototype.bind.call(
      // eslint-disable-next-line @typescript-eslint/no-empty-function
      () => {},
      // eslint-disable-next-line @typescript-eslint/no-empty-function
      () => {}
    );
  })();

  // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
  private debugLogFunction = (function() {
    return Function.prototype.bind.call(console.log, console);
  })();

  private log = this.initLogFunction;

  private renderer?: THREE.WebGLRenderer;
  private scene?: THREE.Scene;
  private camera?: THREE.PerspectiveCamera;
  private orbitControls?: OrbitControls;
  private gridHelper?: THREE.GridHelper;

  constructor() {
    super();
    this.width = 800;
    this.height = 500;
    this.scaleFactor = 1.0;
    this.debug = false;
    this.groundColorHex = 0x000000;
    this.backgroundColorHex = 0xdefcff;
    this.translationVector = { x: 0.0, y: 0.0, z: 0.0 };
    this.rotationVector = { x: 0.0, y: 0.0, z: 0.0 };
  }

  connectedCallback(): void {
    super.connectedCallback();
    if (this.debug) {
      this.log = this.debugLogFunction;
    }
    this.log('Execute connectedCallback');
    if (this.gltfFile) {
      this.loadGltf();
    }
  }

  updated(properties: Map<string, string>): void {
    if (properties.has('object')) {
      this.objectChanged(this.object);
    } else if (properties.has('debug')) {
      this.switchDebug(this.debug || false);
    }
  }

  private switchDebug(debug: boolean): void {
    if (debug) {
      this.log = this.debugLogFunction;
      this.optionallyAddGridHelper(this.scene);
    } else {
      this.optionallyRemoveGridHelper(this.scene);
      this.log = this.initLogFunction;
    }
  }

  private objectChanged(object: GLTF): void {
    this.log(object);
    this.log('Object loaded now render scene');
    this.renderSceneWith(object.scene);
  }

  private renderSceneWith(loadedObject: THREE.Group): void {
    const objectPosition = new THREE.Vector3(0.0, 0.0, 0.0);
    this.camera = this.createCamera(objectPosition);
    this.scene = this.createScene();

    // adjust object and add to scene
    loadedObject = this.adjustObject(loadedObject, objectPosition);
    this.scene.add(loadedObject);

    this.addLights(this.scene);
    this.addPlane(this.scene);
    this.optionallyAddGridHelper(this.scene);

    this.renderer = this.createRenderer();
    this.renderer.render(this.scene, this.camera);

    this.orbitControls = this.createOrbitControls(this.camera, this.renderer);
    this.animateScene();
  }

  private animateScene(): void {
    requestAnimationFrame(() => this.animateScene());
    if (this.orbitControls && this.doCircle) {
      this.orbitControls.update();
    }
    if (this.camera && this.scene && this.renderer) {
      this.renderer.render(this.scene, this.camera);
    }
  }

  private createOrbitControls(camera: THREE.Camera, render: THREE.WebGLRenderer): OrbitControls {
    this.log('Create OrbitControls');
    const orbitControls = new OrbitControls(camera, render.domElement);
    orbitControls.autoRotate = true;
    orbitControls.autoRotateSpeed = CircleAround.CIRCLE_SPEED;
    return orbitControls;
  }

  private createRenderer(): THREE.WebGLRenderer {
    this.log('Create WebGLRenderer');
    const renderer = new THREE.WebGLRenderer({ antialias: true });
    renderer.setSize(this.width, this.height);
    renderer.setClearColor(this.backgroundColorHex, 1);
    // we create the canvas based on the renderers dom element
    this.canvas = renderer.domElement;
    return renderer;
  }

  private optionallyAddGridHelper(scene: THREE.Scene | undefined): void {
    if (scene && this.debug && !this.gridHelper) {
      this.log('Add grid helper');
      this.gridHelper = new THREE.GridHelper(1200, 60, 0xff4444, 0x404040);
      scene.add(this.gridHelper);
    }
  }

  private optionallyRemoveGridHelper(scene: THREE.Scene | undefined): void {
    if (scene && this.gridHelper) {
      this.log('Remove grid helper');
      scene.remove(this.gridHelper);
      this.gridHelper = undefined;
    }
  }

  private addPlane(scene: THREE.Scene): void {
    let material = new THREE.MeshBasicMaterial({ color: this.groundColorHex, side: THREE.DoubleSide });
    if (this.groundTexture) {
      const texture = THREE.ImageUtils.loadTexture(this.groundTexture);
      texture.wrapS = THREE.RepeatWrapping;
      texture.wrapT = THREE.RepeatWrapping;
      texture.repeat.set(40, 40);
      material = new THREE.MeshLambertMaterial({ map: texture });
    }

    const geometry = new THREE.PlaneGeometry(this.width * 10, this.height * 10, 1);
    geometry.rotateX(-Math.PI * 0.5);
    const plane = new THREE.Mesh(geometry, material);
    scene.add(plane);
  }

  private addLights(scene: THREE.Scene): void {
    this.log('Add lights');
    const ambientLight = new THREE.AmbientLight(0xfcfbe6, 0.5);
    ambientLight.position.y = 180;
    ambientLight.position.z = 0;
    scene.add(ambientLight);

    const directionalLight = new THREE.DirectionalLight(0xffffff, 0.5);
    directionalLight.position.set(100, 50, -100);
    scene.add(directionalLight);
  }

  private adjustObject(loadedObject: THREE.Group, initialPosition: THREE.Vector3): THREE.Group {
    this.log('Set object position');
    initialPosition = initialPosition.add(
      new Vector3(this.translationVector.x, this.translationVector.y, this.translationVector.z)
    );
    loadedObject.position.copy(initialPosition);
    loadedObject.rotateX(this.rotationVector.x);
    loadedObject.rotateY(this.rotationVector.y);
    loadedObject.rotateZ(this.rotationVector.z);
    this.log('Scale object');
    loadedObject.scale.copy(new THREE.Vector3(this.scaleFactor, this.scaleFactor, this.scaleFactor));
    return loadedObject;
  }

  private createCamera(cameraTarget: THREE.Vector3): THREE.PerspectiveCamera {
    this.log('Create PerspectiveCamera');
    const camera = new THREE.PerspectiveCamera(50, 1, 0.1, 2000);
    camera.position.copy(new THREE.Vector3(0.0, 220.0, 600.0));
    camera.lookAt(cameraTarget);
    camera.updateProjectionMatrix();
    return camera;
  }

  private createScene(): THREE.Scene {
    this.log('Create Scene');
    return new THREE.Scene();
  }

  private loadGltf(): void {
    this.log('Load .gltf file');
    const loader = new GLTFLoader();

    // Optional:
    const dracoLoader = new DRACOLoader();
    dracoLoader.setDecoderPath('three/examples/jsm/libs/draco/');
    loader.setDRACOLoader(dracoLoader);

    // gltfFile
    loader.load(
      // resource URL
      this.gltfFile,
      // called as soon as the resource is loaded
      (gltf) => {
        this.object = gltf;
      },
      // called while loading is progressing
      (xhr) => {
        if (xhr.loaded && xhr.total) {
          this.log((xhr.loaded / xhr.total) * 100 + '% loaded');
        }
      },
      // called when loading has errors
      (err) => {
        console.error('An error happened loading .gltf file. Debug to gain more information. ' + err);
      }
    );
  }

  private circleModeChanged(event: CustomEvent): void {
    this.log(`switched do circle: ${event.detail.isOn}`);
    if (event.detail.isOn) {
      this.doCircle = true;
    } else {
      this.doCircle = false;
    }
  }

  render(): TemplateResult {
    return html`
      <div class="main-container">
        ${this.canvas} ${this.object ? this.renderBtn() : this.renderLoad()}
      </div>
    `;
  }

  renderLoad(): TemplateResult {
    return html`
      <mwc-linear-progress style="width: ${this.width}px" class="progress" indeterminate></mwc-linear-progress>
    `;
  }

  renderBtn(): TemplateResult {
    return html`
      <div class="button-container">
        <mwc-icon-button-toggle on @MDCIconButtonToggle:change="${this.circleModeChanged}">
          <svg slot="offIcon" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
            <path
              fill="#000000"
              stroke="#000"
              stroke-width="1.5"
              style="pointer-events:inherit"
              d="M0 0 L24 12 L0 24 Z"
            ></path>
          </svg>
          <svg slot="onIcon" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
            <rect
              fill="#000000"
              stroke="#000"
              stroke-width="1.5"
              style="pointer-events:inherit"
              width="25"
              height="25"
              id="svg_1"
              stroke-dasharray="none"
              fill-opacity="1"
            ></rect>
          </svg>
        </mwc-icon-button-toggle>
      </div>
    `;
  }

  static get styles(): CSSResult {
    return css`
      .main-container {
        display: flex;
        flex-direction: column;
        align-items: center;
      }

      .button-container {
        padding: 20px;
        width: 50px;
        height: 50px;
      }

      .progress {
        height: 50px;
      }
    `;
  }
}
