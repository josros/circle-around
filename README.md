# Circle around component

The circle around components is a simple demonstration of a [Web Component](https://github.com/w3c/webcomponents) with the ability to circle around a loaded 3D model.

<img src="./circle-around-1.jpg" height=300px>

You can use the component as follows:

```html
<circle-around 
    gltfFile="./local/path/to/your/model.gltf" 
></circle-around>
```

Have a look at the [demo](https://josros.gitlab.io/circle-around-component/)

## Execution

```bash
# install dependencies
npm install

# run dev server at http://localhost:3000
npm run serve


# more optional scripts
# production build
npm run build

# Lintting
npm run lint
```

## Load model into demo view

Adjust `gltfFile` in [index.html](index.html)

```html
<circle-around 
    gltfFile="./local/path/to/your/model.gltf" 
    scaleFactor="20"
    groundTexture="./local/path/to/a/ground/texture.jpg"
></circle-around>
```

Depending on your model you may want to ajdust the `scaleFactor` and the `groundTexture` among others. See properties in the [component](src/components/circleAround.ts).

Refresh the page started earlier (via  `npm run serve`).

## Notes

Because of licencing reasons I could not provide 3D models with this repository, but you may find one on: [sketchfab](https://sketchfab.com/feed)

## Maintainer

[Josef Rossa](https://gitlab.com/josros)